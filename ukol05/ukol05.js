exports.hasKey = function (obj, key) {
    if (key in obj) {
        return true;
    }
    return false;
}

exports.hasValue = function (obj, key) {
    if (Object.values(obj).indexOf(key) > -1) {
        return true;
    }
    return false;
}