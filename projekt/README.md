# Konečná verze Tasks toolu
- Verze je již funkční včetně nutnosti přihlášení.

### Projekt je dostupný na:
- [https://tasks.jakubruta.tk](https://tasks.jakubruta.tk)
- Username: Test
- Password: Welcome2019

- Username: Test1
- Password: Welcome2019

#### Json výstupy (nutné přihlášení)
- https://tasks.jakubruta.tk/tasks/filter/all - poslední hodnota je parametr a může nabývat hodnot created_by_me, done, my, closed

Pro případný test notifikací je nutné povolit zasílání notifikací v ldap portálu na [https://ldap.topescape.cz/dashBoard](https://ldap.topescape.cz/dashBoard)
(notifikace fungují pouze v chromu)

