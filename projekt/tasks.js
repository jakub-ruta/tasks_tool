let express = require('express');
let app = express();
const bodyParser = require('body-parser');
app.use(bodyParser.json());       // to support JSON-encoded bodies
app.use(bodyParser.urlencoded({// to support URL-encoded bodies
    extended: true
}));
var cookieParser = require('cookie-parser');
app.use(cookieParser());
var mysql = require('mysql');
var request = require("request");
var striptags = require('striptags');
var session = require('express-session');
var creditals = require('./creditals.js'); //soubor s přístupovými údaji
var db_config = creditals.getDb_config();
var ses_secret = creditals.getSessionSecret();
var con;
/**
 *
 * Funkce pro udržování připojení s databázovým serverem
 */
function handleDisconnect() {
    con = mysql.createConnection(db_config); // Recreate the connection, since
    // the old one cannot be reused.

    con.connect(function (err) {              // The server is either down
        if (err) {                                     // or restarting (takes a while sometimes).

            setTimeout(handleDisconnect, 2000); // We introduce a delay before attempting to reconnect,
        }                                     // to avoid a hot loop, and to allow our node script to
    }); // process asynchronous requests in the meantime.
    // If you're also serving http, display a 503 error.
    con.on('error', function (err) {

        if (err.code === 'PROTOCOL_CONNECTION_LOST') { // Connection to the MySQL server is usually
            handleDisconnect(); // lost due to either server restart, or a
        } else {                                      // connnection idle timeout (the wait_timeout
            throw err; // server variable configures this)
        }
    });
}
/**
 * Volání funkce pro hlídání připojení k databázi a případného znovupřipojení - pro rychlejší práci je stále
 * udržováno aktivní spojení.
 * Funkce nalezena na internetu a použita.
 */
handleDisconnect();
/**
 * Kód pro založení statického servírovaní obsahu - obrázků, scriptů, úvodního html ze složky public
 */
app.use(express.static('public'));
/**
 * Kód pro využití modulu session pro udržení informace o přihlášeném uživateli. (pro potřeby školní práce
 * stačí, pro větší projekt by pak byl implementován systém tokenů s tokeny v databázi - v přecházejících
 * commitech na to byla příprava a skoro funkční řešení.)
 */
app.use(session({secret: ses_secret}));
/**
 * Kód pro založení uživatele v session
 */
app.use(function (req, res, next) {
    if (!req.session.user) {
        req.session.user = {}
    }
    next()
})
/**
 * Cesta pro ohlášení uživatele
 * vrací přesměrování na ldap přihlášení
 */
app.get('/logout', (req, res) => {
    req.session.user = null;
    res.redirect("https://ldap.topescape.cz/login/remote/Tasks%20tool//");

});

/**
 * Cesta pro získání informací o přihlášeném uživateli.
 * vrací uživatele
 */
app.get('/users/loggedin', verify_user, (req, res) => {
    con.query("SELECT * FROM users WHERE user_id=?", [req.session.user.user_id], function (err, result, fields) {
        if (err)
            throw err;

        res.json((result));
    });
});
/**
 * Cesta pro přihlášení (založení) uživatele pomocí tokenu, který dá ldap
 * pomocí tokenu se zeptá ldap na data uživatele, v případě, že lokálně neexistuje, založí se, v opačném případě
 * se aktualizuje a poté se přihlásí.
 * na tuto adresu přesměrovává ldap po schválení přihlášení
 * vrací session cookie
 * v případě chyby vrací unauthorized
 */
app.get('/login/ldap/:token', (req, res) => {
    con.query("SELECT * FROM internal_remote_token WHERE adress=?", ['ldap.topescape.cz'], function (err, result, fields) {
        if (err)
            throw err;

        var ldap_login_url = "https://ldap.topescape.cz/ajax/get/userLogin?app_token=" + result[0].token +
                "&user_token=" + req.params.token;

        console.log(req.params.token)
        request({
            url: ldap_login_url,
            json: true,
        }, function (error, response, body) {



            if (!error && response.statusCode === 200) {
                // Print the json response
                con.query("SELECT * FROM users WHERE user_id=?", [body.data.user_id], function (err, result, fields) {
                    if (err)
                        throw err;

                    ////////////////////////////
                    try {
                        typeof result[0].user_id !== 'undefined'
                        var user = result[0];
                        con.query("UPDATE users SET login_name=?, nickname=?, mail=?, locale=?, avatar=?, admin_level=?  WHERE user_id=?"
                                , [body.data.login_name, body.data.nickname, body.data.mail, body.data.locale, body.data.avatar, body.data.admin_level, user.user_id], function (err, result, fields) {

                            if (err) {

                            } else {


                                con.query("SELECT * FROM users WHERE user_id=?", [user.user_id], function (err, result, fields) {
                                    if (err)
                                        throw err;

                                    req.session.user = user;
                                    res.redirect("https://tasks.jakubruta.tk");
                                });
                            }
                        });
                    } catch (err) {
                        //registrace a přihlášení
                        var user = body.data;
                        con.query("INSERT INTO users (login_name, nickname, mail, locale, avatar, user_id, admin_level) VALUES(?,?,?,?,?,?,?)  \n\
 ", [user.login_name, user.nickname, user.mail, user.locale, user.avatar, user.user_id, user.admin_level], function (err, result, fields) {
                            if (err) {

                            } else {
                                con.query("SELECT * FROM users WHERE user_id=?", [user.user_id], function (err, result, fields) {
                                    if (err)
                                        throw err;

                                    req.session.user = user;
                                    res.redirect("https://tasks.jakubruta.tk");
                                });
                            }
                        });
                    }
                });
                //////////

            } else {
                res.status(401).json({status: "remote Unauthorized", message: "Token probably expired"});
            }
        });
    }
    );
});

/**
 * Metoda pro ldap - využívá přehled aplikací
 * vrací počet otevřených a přijatých tasků
 */
app.post('/ajax/get/local/data', (req, res) => {
    var accepted = 0;
    var open = 0;

    con.query("SELECT COUNT(*) AS count FROM tasks WHERE assigned_user_id=? AND status=?", [req.body.user_id, "Open"], function (err, result, fields) {
        if (err)
            throw err;

        open = result[0].count;
        con.query("SELECT COUNT(*) AS count FROM tasks WHERE assigned_user_id=? AND status=?", [req.body.user_id, "Accepted"], function (err, result, fields) {
            if (err)
                throw err;

            accepted = result[0].count;
            var result = {
                "status": "success",
                "data": {0: {0: "Open", 1: open, 2: "Open my tasks", 3: "green"},
                    1: {0: "Pending", 1: accepted, 2: "Accepted my tasks", 3: "blue"}}
            };
            res.json((result));
        });
    });
}
);

/**
 * Cesta pro získání všech uživatelů pro přířazení uživatele při zakládání tasku
 */
app.get('/users', verify_user, (req, res) => {
    con.query("SELECT user_id, login_name, nickname, disabled FROM users WHERE 1 ORDER BY login_name ASC", function (err, result, fields) {
        if (err)
            throw err;

        res.json((result));
    });
});

/**
 * Cesta pro získání tasků podle filtru
 * vrací tasky
 */
app.get('/tasks/filter/:filter', verify_user, (req, res) => {
    var creator = '%';
    var assigned = '%';
    var status = '%';
    var status_not1 = 1;
    var status_not2 = 1;
    switch (req.params.filter)
    {

        case 'created_by_me':
            creator = req.session.user.user_id;
            break;
        case 'my':
            status_not1 = 'Closed';
            status_not2 = 'Done';
            assigned = req.session.user.user_id;
            break;
        case 'done':

            status = 'Done';
            break;
        case 'closed':

            status = 'Closed';
            break;
        default :

            break;
    }
    con.query("SELECT tasks.*, users.*, creator.login_name AS creator_login_name, creator.user_id AS creator_user_id, creator.nickname AS creator_nickname, creator.mail AS creator_mail, creator.avatar as creator_avatar FROM `tasks` LEFT JOIN users ON users.user_id=tasks.assigned_user_id LEFT JOIN users AS creator ON creator.user_id=tasks.creator_user_id WHERE creator_user_id LIKE ? AND assigned_user_id LIKE ? AND status LIKE ? AND status NOT LIKE ? AND status NOT LIKE ?", [creator, assigned, status, status_not1, status_not2], function (err, result, fields) {
        if (err)
            throw err;
        //
        res.json((result));
    });
});

/**
 * Cesta pro získání jednoho tasku
 * vreací task
 */
app.get('/tasks/:task_id', verify_user, (req, res) => {
    con.query("SELECT tasks.*, users.*, creator.login_name AS creator_login_name, creator.user_id AS creator_user_id, creator.nickname AS creator_nickname, creator.mail AS creator_mail, creator.avatar as creator_avatar FROM `tasks` LEFT JOIN users ON users.user_id=tasks.assigned_user_id LEFT JOIN users AS creator ON creator.user_id=tasks.creator_user_id WHERE tasks.id=?", [req.params.task_id], function (err, result, fields) {
        if (err)
            throw err;

        res.json((result));
    });
});

/**
 * Cesta pro vložení nového tasku. Využívá striptags pro odstranění html tagů.
 * vrací ok a posílá notifikaci
 * vrací error v případě chyby
 */
app.post('/tasks', verify_user, (req, res) => {
    var creator = req.session.user.user_id;
    striptags(req.body);
    con.query("INSERT INTO tasks (task_name, task_note, Type, deathline, creator_user_id, assigned_user_id) \n\
 VALUES (?,?,?,?,?,?)", [striptags(req.body.task_name), striptags(req.body.task_note), striptags(req.body.Type),
        req.body.deathline, creator, req.body.assigned_user_id], function (err, result, fields) {
        if (err) {
            res.status(500).json({status: "error", er: err})
        } else {
            sendPushNotofication(req.body.assigned_user_id, "New task", req.body.task_name);
            res.status(200).json({status: "ok"});

        }

    });

});

/**
 * Cesta pro úpravu nového tasku. Využívá se modulu striptags pro filtraci vložení scriptu do html
 * vrací status ok v případě úspěšné úpravy nebo error v případě chyby
 */
app.put('/tasks/:task_id', verify_user, (req, res) => {

    con.query("UPDATE tasks SET task_name=?, task_note=?, deathline=?  WHERE id=? \n\
", [striptags(req.body.task_name), striptags(req.body.task_note), req.body.deathline,
        req.params.task_id], function (err, result, fields) {
        if (err) {
            res.status(204).json({status: "error", er: err})

        } else {

            res.status(200).json({status: "ok"});
        }

    });

});

/**
 * Cesta pro změnu statusu tasku
 * vrací status ok v případě úspěšné úpravy nebo error v případě chyby
 */
app.put('/tasks/:task_id/status', verify_user, (req, res) => {

    con.query("UPDATE tasks SET status=? WHERE id=? \n\
", [req.body.new_status, req.params.task_id], function (err, result, fields) {
        if (err) {
            res.status(204).json({status: "error", er: err})

        } else {
            res.status(200).json({status: "ok"});
        }

    });

});

/**
 * cesta pro smazání jednoho tasku (nepředpokládá se, že někdo bude obcházet frontend tlačítka, proto
 * pro smazání stačí pouze přihlášený uživatel
 * vrací počet smazaných řádků
 */
app.delete('/tasks/:task_id', verify_user, (req, res) => {
    con.query("DELETE FROM tasks WHERE tasks.id=?", [req.params.task_id], function (err, result, fields) {
        if (err)
            throw err;

        res.json((result));
    });
});

/**
 * Založení cesty pro update tokenu pro autorizaci proti ldap
 */
app.post('/ajax/set/local/token', (req, res) => {

    con.query("SELECT * FROM internal_remote_token WHERE function=? AND adress=?", ["setup", 'ldap.topescape.cz'], function (err, result, fields) {
        if (err)
            throw err;
        if (result[0].token === req.body.auth_token) {
            con.query("UPDATE internal_remote_token SET token=? WHERE function=? AND adress=? \n\
", [req.body.new_token, req.body.type, "ldap.topescape.cz"], function (err, result, fields) {
                if (err)
                    throw err;

                con.query("UPDATE internal_remote_token SET token=? WHERE token=? AND function=? AND adress=? \n\
", [req.body.new_auth_token, req.body.auth_token, "setup", "ldap.topescape.cz"], function (err, result, fields) {
                    if (err)
                        throw err;
                    res.json({status: "success"});
                });


            });
        } else {
            res.status(401).json({status: "Unauthorized"});
        }
    });
});
/**
 * Spuštění serveru na portu 4000 (z veřejného internetu tento port nefunguje a je nutné přistupovat přes
 * proxy na adrese https://tasks.jakubruta.tk, který zajišťuje https provoz
 */
app.listen(4000, (err) => {
    if (err)
        throw err;

});

/**
 * Metoda pro oveření, že je uživatel přihlášen. V případě, že ne, pošle klientovi unauthorized
 * @param {type} req
 * @param {type} res
 * @param {type} next
 * @returns {undefined}
 */
function verify_user(req, res, next) {

    try {
        if (typeof req.session.user.user_id !== 'undefined' && typeof req.session.user.user_id !== null) {
            next();
        } else {
            res.sendStatus(401);
        }
    } catch (Exception) {
        res.sendStatus(401);
    }
}
/**
 * Metoda pro odeslání požadavku na zaslání PUSH notifikace přiřazenému uživateli
 * @param {type} user_id
 * @param {type} title
 * @param {type} body
 * @returns {undefined}
 */
function sendPushNotofication(user_id, title, body) {
    con.query("SELECT * FROM internal_remote_token WHERE function=? AND adress=?", ["send", 'ldap.topescape.cz'], function (err, result, fields) {
        if (err)
            throw err;
        else {
            var ldap_push_url = 'https://ldap.topescape.cz/ajax/send/PUSH'
            request.post({
                headers: {'content-type': 'application/x-www-form-urlencoded'},
                url: ldap_push_url,
                body: 'push_user_id=' + user_id + '&push_message=' + body + '&push_title=' + title + '&token=' + result[0].token,
            }, function (error, response, body) {
                console.log(body)
            });
        }
    });
}