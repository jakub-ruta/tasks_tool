var logged_user;
var last_filter = "my";
var ldap_login_url = 'https://ldap.topescape.cz/login/remote/Tasks%20tool//';

$(document).ready(function get_logged_user() {
    $.ajax({
        type: 'GET',
        url: 'https://tasks.jakubruta.tk/users/loggedin',
        contentType: 'application/json',
    }).done(function (data, status, jqXHR) {
        logged_user = data[0];
        $('#logged_user_name').text(logged_user.login_name);
        if (logged_user.admin_level > 2) {
            $('.panel-tabs').append('<a  id="all_task" onclick="get_tasks(\'all\');">All</a>');
        }

        get_tasks(last_filter);
    }).fail(function (msg) {
        if (msg.status === 401) {
            window.location.href = ldap_login_url;
        }
        addNotification("No user is logged in!", "danger", 12000, "center");
    })
});
function update_task() {
    id = $('#task_id').val();
    $.ajax({
        type: 'PUT',
        url: 'https://tasks.jakubruta.tk/tasks/' + id,
        contentType: 'application/json',
        data: JSON.stringify({task_name: $('#task_name').val(),
            task_note: $('#task_note').val(),
            Type: $('#task_type').val(),
            deathline: $('#task_deathline').val()}),
    }).done(function (data, status, jqXHR) {

        addNotification(status, "success", 2000, "top-right");
        change_modal_visibility();
        get_tasks(last_filter);
        get_task(id);
    }).fail(function (msg) {
        if (msg.status === 401) {
            window.location.href = ldap_login_url;
        }

    })
}


function create_task() {
    $.ajax({
        type: 'POST',
        url: 'https://tasks.jakubruta.tk/tasks',
        contentType: 'application/json',
        data: JSON.stringify({task_name: $('#task_name').val(),
            task_note: $('#task_note').val(),
            Type: $('#task_type').val(),
            deathline: $('#task_deathline').val(),
            assigned_user_id: $('#task_assigned_user').val()}), // access in body
    }).done(function (data, status, jqXHR) {

        addNotification(status, "success", 2000, "top-right");
        change_modal_visibility();
        get_tasks();
    }).fail(function (msg) {
        if (msg.status === 401) {
            window.location.href = ldap_login_url;
        }

    })
}
function get_task(id) {
    $.ajax({
        type: 'GET',
        url: 'https://tasks.jakubruta.tk/tasks/' + id,
        contentType: 'application/json',
    }).done(function (data, status, jqXHR) {
        $('.modal-card-title').html('Task view ' + generate_task_status_html(data[0].status));
        $('.modal-card-body').html(fill_task_modal(data[0]));
        $('.modal-card-foot').html(build_task_buttons(data[0]));

    }).fail(function (msg) {
        if (msg.status === 401) {
            window.location.href = ldap_login_url;
        }

    })
}
function get_tasks($filter = 'my') {
    last_filter = $filter;
    $('tbody').html('<td colspan="7"><progress class="progress is-small is-primary" max="100">15%</progress></td>');
    $('.panel-tabs a').removeClass('is-active');
    $('#' + $filter + '_task').addClass('is-active');
    $.ajax({
        type: 'GET',
        url: 'https://tasks.jakubruta.tk/tasks/filter/' + $filter,
        contentType: 'application/json',
    }).done(function (data, status, jqXHR) {
        $('tbody').text('');
        $(data).each(function (index, item) {
            $('tbody').append(build_tasks_html(item));
        });
    }).fail(function (msg, status) {
        if (msg.status === 401) {
            window.location.href = ldap_login_url;
        }

    })
}
function delete_task(id) {
    $.ajax({
        type: 'DELETE',
        url: 'https://tasks.jakubruta.tk/tasks/' + id,
        contentType: 'application/json',
    }).done(function (data, status, jqXHR) {
        get_tasks(last_filter);

    }).fail(function (msg) {
        if (msg.status === 401) {
            window.location.href = ldap_login_url;
        }

        addNotification("task deletion failed", "danger", 5000, "center");

    })

}

function build_tasks_html(item) {
    $html = '<tr>';
    $html += '<th onclick="show_task(' + item.id + ')">' + item.task_name + '</th>';
    $html += '<td class="is-hidden-mobile">' + new Date(item.created).toLocaleDateString('en-EN') + '</td>';
    $html += '<td>' + item.Type + '</td>';
    $html += '<td>' + generate_task_status_html(item.status) + '</td>';
    $html += '<td>' + item.creator_login_name + '</td>';
    $html += '<td>' + item.deathline + '</td>';
    $html += '<td>' + item.login_name + '</td>';
    $html += '<td><button class="button is-small is-info is-light" onclick="show_task(' + item.id + ')">Open</button>';
    if (item.creator_user_id === logged_user.user_id || logged_user.admin_level > 2) {
        $html += '<button class="button is-small is-danger is-light" onclick="delete_task(' + item.id + ')">Delete</button>';
    }
    $html += '</td>';
    $html += '</tr>';
    return $html;
}

function show_task(id) {
    change_modal_visibility();
    $('.modal-card-title').html('Task view');
    $('.modal-card-body').html('<progress class="progress is-small is-primary" max="100">15%</progress>');
    get_task(id);
}

function change_modal_visibility() {
    $('.modal').toggleClass('is-active');
    $('html').toggleClass('is-clipped');
}
function changeMenuVisibility() {
    $('.navbar-burger').toggleClass('is-active');
    $('.navbar-menu').toggleClass('is-active');
}

function fill_task_modal(data) {
    $disabled = true;
    if (data.creator_user_id === logged_user.user_id || logged_user.admin_level > 2) {
        $disabled = false;
    }

    $html = '<form>';
    $html += '<input type="hidden" name="task_id" id="task_id" value="' + data.id + '" />';
    $html += text_input_builder('Task name:', 'task_name', data.task_name, $disabled);
    $html += text_input_builder('Task type:', 'task_type', data.Type, true);
    $html += date_input_builder('Task deathline:', 'task_deathline', data.deathline, $disabled);
    $html += textarea_input_builder('Task description:', 'task_note', data.task_note, $disabled);
    $html += text_input_builder('Task creator:', 'task_creator', data.creator_login_name, true);
    $html += text_input_builder('Task created:', 'task_created', new Date(data.created).toLocaleDateString('en-EN'), true);
    $html += text_input_builder('Task status:', 'task_status', data.status, true);
    $html += '</form>';
    return $html;
}

function text_input_builder(name, id, value, disabled) {
    if (disabled) {
        $disabled_html = 'readonly';
    } else {
        $disabled_html = '';
    }
    return '<div class="field">' +
            '<label class="label">' + name + '</label>' +
            '<div class="control">' +
            ' <input class="input" name="' + id + '" id="' + id + '" type="text" ' + $disabled_html + ' value="' + value + '" />' +
            '</div>' +
            '</div>';
}
function date_input_builder(name, id, value, disabled) {
    if (disabled) {
        $disabled_html = 'readonly';
    } else {
        $disabled_html = '';
    }
    return '<div class="field">' +
            '<label class="label">' + name + '</label>' +
            '<div class="control">' +
            ' <input class="input" name="' + id + '" id="' + id + '" type="date" ' + $disabled_html + ' value="' + value + '" />' +
            '</div>' +
            '</div>';
}
function textarea_input_builder(name, id, value, disabled) {
    if (disabled) {
        $disabled_html = 'readonly';
    } else {
        $disabled_html = '';
    }
    return '<div class="field">' +
            '<label class="label">' + name + '</label>' +
            '<div class="control">' +
            '<textarea class="textarea" name="' + id + '" id="' + id + '" ' + $disabled_html + '>' + value + '</textarea>' +
            '</div>' +
            '</div>';
}
/**
 * Funkce pro vytvoření html pro tlačítka k úkolu
 * Tvůrce úkolu může úkol smazat a označit za uzavřený a upravovat úkol
 * Přiřazený uživatel může přijmout nebo odmítnout úkol. Přijmutý může označit za hotový
 * Admin může provádě všechny operace + znovu otevřít úkol
 * @param {type} item
 * @returns {nm$_tasks_user.$html|String}
 */
function build_task_buttons(item) {
    $html = '';
    if ((item.creator_user_id === logged_user.user_id && item.status !== "Closed") || logged_user.admin_level > 2) {
        $html += '<button class="button is-success is-small" onclick="update_task();">Save changes</button>';
        $html += '<button class="button is-info is-small" onclick="change_status_task(\'Closed\');">Set task as Closed</button>';
        if (logged_user.admin_level > 2) {
            $html += '<button class="button is-info is-small" onclick="change_status_task(\'Open\');">Reopen</button>';
        }
    }
    if (item.assigned_user_id === logged_user.user_id && item.status === "Open" || logged_user.admin_level > 2) {
        $html += '<button class="button is-primary is-small" onclick="change_status_task(\'Accepted\');">Accept task</button>';
        $html += '<button class="button is-warning is-small" onclick="change_status_task(\'Rejected\');">Reject task</button>';
    }
    if (item.assigned_user_id === logged_user.user_id && item.status === "Accepted" || logged_user.admin_level > 2) {
        $html += '<button class="button is-success is-small" onclick="change_status_task(\'Done\');">Set task as done</button>';
    }
    $html += '<button class="button is-small" onclick="change_modal_visibility();">Close</button>';
    return $html;
}

function generate_task_status_html(status) {
    switch (status) {
        case 'Open':
            return '<span class="tag is-warning">' + status + '</span>'
            break;
        case 'Accepted':
            return '<span class="tag is-info">' + status + '</span>'
            break;
        case 'Rejected':
            return '<span class="tag is-danger">' + status + '</span>'
            break;
        case 'Done':
            return '<span class="tag is-success">' + status + '</span>'
            break;
        case 'Closed':
            return '<span class="tag is-primary">' + status + '</span>'
            break;
        default:
            return '<span class="tag is-black">' + status + '</span>'
            break;
    }
}

function change_status_task(new_status) {
    $task_id = $('#task_id').val();
    $.ajax({
        type: 'PUT',
        url: 'https://tasks.jakubruta.tk/tasks/' + $task_id + "/status",
        contentType: 'application/json',
        data: JSON.stringify({
            new_status: new_status}), // access in body
    }).done(function (data, status, jqXHR) {
        change_modal_visibility();
        get_tasks(last_filter);
        show_task($task_id);


    }).fail(function (msg) {
        addNotification("task status change failed", "danger", 5000, "center");

    })
}

function create_task_modal() {
    change_modal_visibility();
    $disabled = false;
    $html = '<form>';
    $html += text_input_builder('Task name:', 'task_name', "", $disabled);
    $html += text_input_builder('Task type:', 'task_type', "Important", $disabled);

    $html += date_input_builder('Task deathline:', 'task_deathline', "", $disabled);
    $html += select_input_user_builder("Assigned user", "task_assigned_user");

    $html += textarea_input_builder('Task description:', 'task_note', "", $disabled);
    $html += '</form>';
    $('.modal-card-title').html('Create new task');
    $('.modal-card-body').html($html);
    $('.modal-card-foot').html(create_button_new_task());
    add_user_to_select();
}

function create_button_new_task() {
    $html = '<button class="button is-success is-small" onclick="create_task();">Create task</button>';
    $html += '<button class="button is-small" onclick="change_modal_visibility();">Close</button>';
    return $html;
}
function select_input_user_builder(label, id) {

    return '<div class="field">' +
            '<label class="label">' + label + '</label>' +
            '<div class="control select">' +
            '<select name="' + id + '" id="' + id + '">' +
            '<option value="' + logged_user.user_id + '">' + logged_user.login_name + '</option>' +
            '</select>' +
            '</div>' +
            '</div>';
}

function add_user_to_select() {
    $.ajax({
        type: 'GET',
        url: 'https://tasks.jakubruta.tk/users',
        contentType: 'application/json',
    }).done(function (data, status, jqXHR) {
        $('#task_assigned_user').html('');
        $(data).each(function (index, item) {
            $('#task_assigned_user').append('<option value="' + item.user_id + '" ' + (item.disabled ? "disabled" : "") + '>' + item.login_name + (item.disabled ? " (disabled user)" : "") + '</option>');
        });
    }).fail(function (msg) {

    })
}


function addNotification($message = "Notification test", $color = "danger", $visible_for = 2000, $position = "top-right") {
    bulmaToast.toast({
        message: $message,
        duration: $visible_for,
        position: $position,
        type: "is-" + $color,
        dismissible: true,
        pauseOnHover: true,
        animate: {in: "fadeIn", out: "fadeOut"}
    });
}

/*
 * Využití vytvořeného systému notifikací v aplikaci formou vyskakovacího okénka
 *
 * bulma-toast 1.5.4
 * (c) 2018-present @rfoel <rafaelfr@outlook.com>
 * Released under the MIT License.
 */
(function (a, b) {
    "object" == typeof exports && "undefined" != typeof module ? b(exports) : "function" == typeof define && define.amd ? define(["exports"], b) : (a = a || self, b(a.bulmaToast = {}))
})(this, function (a) {
    'use strict';
    function b(a, b) {
        if (!(a instanceof b))
            throw new TypeError("Cannot call a class as a function")
    }
    function c(a, b) {
        for (var c, d = 0; d < b.length; d++)
            c = b[d], c.enumerable = c.enumerable || !1, c.configurable = !0, "value"in c && (c.writable = !0), Object.defineProperty(a, c.key, c)
    }
    function d(a, b, d) {
        return b && c(a.prototype, b), d && c(a, d), a
    }
    function e() {
        j = {noticesTopLeft: l.createElement("div"), noticesTopRight: l.createElement("div"), noticesBottomLeft: l.createElement("div"), noticesBottomRight: l.createElement("div"), noticesTopCenter: l.createElement("div"), noticesBottomCenter: l.createElement("div"), noticesCenter: l.createElement("div")};
        for (var a in j.noticesTopLeft.setAttribute("style", "".concat("width:100%;z-index:99999;position:fixed;pointer-events:none;display:flex;flex-direction:column;padding:15px;", "left:0;top:0;text-align:left;align-items:flex-start;")), j.noticesTopRight.setAttribute("style", "".concat("width:100%;z-index:99999;position:fixed;pointer-events:none;display:flex;flex-direction:column;padding:15px;", "right:0;top:0;text-align:right;align-items:flex-end;")), j.noticesBottomLeft.setAttribute("style", "".concat("width:100%;z-index:99999;position:fixed;pointer-events:none;display:flex;flex-direction:column;padding:15px;", "left:0;bottom:0;text-align:left;align-items:flex-start;")), j.noticesBottomRight.setAttribute("style", "".concat("width:100%;z-index:99999;position:fixed;pointer-events:none;display:flex;flex-direction:column;padding:15px;", "right:0;bottom:0;text-align:right;align-items:flex-end;")), j.noticesTopCenter.setAttribute("style", "".concat("width:100%;z-index:99999;position:fixed;pointer-events:none;display:flex;flex-direction:column;padding:15px;", "top:0;left:0;right:0;text-align:center;align-items:center;")), j.noticesBottomCenter.setAttribute("style", "".concat("width:100%;z-index:99999;position:fixed;pointer-events:none;display:flex;flex-direction:column;padding:15px;", "bottom:0;left:0;right:0;text-align:center;align-items:center;")), j.noticesCenter.setAttribute("style", "".concat("width:100%;z-index:99999;position:fixed;pointer-events:none;display:flex;flex-direction:column;padding:15px;", "top:0;left:0;right:0;bottom:0;flex-flow:column;justify-content:center;align-items:center;")), j)
            l.body.appendChild(j[a]);
        k = {"top-left": j.noticesTopLeft, "top-right": j.noticesTopRight, "top-center": j.noticesTopCenter, "bottom-left": j.noticesBottomLeft, "bottom-right": j.noticesBottomRight, "bottom-center": j.noticesBottomCenter, center: j.noticesCenter}, i = !0
    }
    function f(a) {
        i || e();
        var b = Object.assign({}, h, a), c = new m(b), d = k[b.position] || k[h.position];
        d.appendChild(c.element)
    }
    function g(a) {
        for (var b in j) {
            var c = j[b];
            c.parentNode.removeChild(c)
        }
        l = a, e()
    }
    var h = {message: "Your message here", duration: 2e3, position: "top-right", closeOnClick: !0, opacity: 1}, i = !1, j = {}, k = {}, l = document, m = /*#__PURE__*/function () {
        function a(c) {
            var d = this;
            b(this, a), this.element = l.createElement("div"), this.opacity = c.opacity, this.type = c.type, this.animate = c.animate, this.dismissible = c.dismissible, this.closeOnClick = c.closeOnClick, this.message = c.message, this.duration = c.duration, this.pauseOnHover = c.pauseOnHover;
            var e = "width:auto;pointer-events:auto;display:inline-flex;white-space:pre-wrap;opacity:".concat(this.opacity, ";"), f = ["notification"];
            if (this.type && f.push(this.type), this.animate && this.animate["in"] && (f.push("animated ".concat(this.animate["in"])), this.onAnimationEnd(function () {
                return d.element.classList.remove(d.animate["in"])
            })), this.element.className = f.join(" "), this.dismissible) {
                var g = l.createElement("button");
                g.className = "delete", g.addEventListener("click", function () {
                    d.destroy()
                }), this.element.insertAdjacentElement("afterbegin", g)
            } else
                e += "padding: 1.25rem 1.5rem";
            this.closeOnClick && this.element.addEventListener("click", function () {
                d.destroy()
            }), this.element.setAttribute("style", e), "string" == typeof this.message ? this.element.insertAdjacentHTML("beforeend", this.message) : this.element.appendChild(this.message);
            var h = new n(function () {
                d.destroy()
            }, this.duration);
            this.pauseOnHover && (this.element.addEventListener("mouseover", function () {
                h.pause()
            }), this.element.addEventListener("mouseout", function () {
                h.resume()
            }))
        }
        return d(a, [{key: "destroy", value: function () {
                    var a = this;
                    this.animate && this.animate.out ? (this.element.classList.add(this.animate.out), this.onAnimationEnd(function () {
                        return a.removeChild(a.element)
                    })) : this.removeChild(this.element)
                }}, {key: "removeChild", value: function (a) {
                    a.parentNode && a.parentNode.removeChild(a)
                }}, {key: "onAnimationEnd", value: function () {
                    var a = 0 < arguments.length && void 0 !== arguments[0] ? arguments[0] : function () {}, b = {animation: "animationend", OAnimation: "oAnimationEnd", MozAnimation: "mozAnimationEnd", WebkitAnimation: "webkitAnimationEnd"};
                    for (var c in b)
                        if (void 0 !== this.element.style[c]) {
                            this.element.addEventListener(b[c], function () {
                                return a()
                            });
                            break
                        }
                }}]), a
    }(), n = /*#__PURE__*/function () {
        function a(c, d) {
            b(this, a), this.timer, this.start, this.remaining = d, this.callback = c, this.resume()
        }
        return d(a, [{key: "pause", value: function () {
                    window.clearTimeout(this.timer), this.remaining -= new Date - this.start
                }}, {key: "resume", value: function () {
                    this.start = new Date, window.clearTimeout(this.timer), this.timer = window.setTimeout(this.callback, this.remaining)
                }}]), a
    }();
    a.setDoc = g, a.toast = f, Object.defineProperty(a, "__esModule", {value: !0})
});