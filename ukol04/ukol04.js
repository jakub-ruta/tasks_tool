var lastColor = "";
var banner;
document.addEventListener('DOMContentLoaded', (event) => {
    var buttons = document.getElementsByTagName("button");
    banner = document.getElementById("banner");
    for (var button of buttons) {
        console.log(button);
        button.addEventListener("click", clickedButton);
    }
})
function clickedButton(e) {
    var button = e.target;
    var color = button.getAttribute("class").substring(25);
    if (lastColor.length > 2) {
        banner.classList.replace("alert-" + lastColor, "alert-" + color);
    } else {
        banner.classList.add("alert-" + color);
    }
    lastColor = color;
}
