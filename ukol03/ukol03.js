var prepinac = document.getElementById("prepinac");
var poznamka = document.querySelectorAll('.poznamka');
prepinac.onclick = function () {
    myFunction()
};
function myFunction() {
    if (prepinac.innerHTML === 'skrýt poznámky') {
        for (let el of poznamka)
            el.style.visibility = 'hidden';
        prepinac.innerHTML = "zobrazit poznámky";
    } else {
        for (let el of poznamka)
            el.removeAttribute("style");
        prepinac.innerHTML = "skrýt poznámky";
    }
}