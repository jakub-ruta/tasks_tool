
## drawio
- [https://github.com/jgraph/drawio](https://github.com/jgraph/drawio)
- Projekt zaměřen na vytvoření jednoduchého nástroje pro kreslení schémat přímo v porhlížeči.
- Jeho úspěšnost je dána jednoduchostí kreslení, diagramy umožňuje ukládat na google druve, OneDrive
a i do zařízení, je v několika jazycích

## react-table
- [https://github.com/tannerlinsley/react-table](https://github.com/tannerlinsley/react-table)
- Rozšíření pro populární framework react, které umožňuje třídění, řazení úpravy a celkově práci s tabulkami
v prohlížeči.
- Jeho úspěšnost je bude dána právě zrychlením vytvoření nástroje na zobrazení tabulky pro webové vývojáře

## vue
- [https://github.com/vuejs/vue](https://github.com/vuejs/vue)
- Vue je nyní asi nejrozšířenějším frameworkem pro webové aplikace. Pomocí něj lze rychle vytvořit
jednostránkovou rozsáhlou aplikaci
- Jeho úspěšnost je bude dána právě rozšířením a asi jednoduchostí vytvoření aplikace